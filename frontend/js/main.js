/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

$(function() {

    // obtaining DOM elements from the Chat Interface
    const $messageForm = $('#message-form');
    const $messageBox = $('#message');
    const $chat = $('#chat');

    // obtaining DOM elements from the NicknameForm Interface
    const $nickForm = $('#nickForm')
    const $nickError = $('#nickError')
    const $nickname = $('#nickname')
    const $users = $('#usernames')

    let ws

    function init() {
      if (ws) {
        ws.onerror = ws.onopen = ws.onclose = null;
        ws.close();
      }
      ws = new WebSocket('ws://localhost:6969');
      ws.onopen = () => {
        console.log('Connection opened!');
      }
      ws.onmessage = ({ data }) => checkMessage(data)
      ws.onclose = function() {
        ws = null;
      }
    }

    function getUsernames (users) {
        var elements = ''
        let user = ''
        let html = '';
        for(i = 0; i < users.length; i++) {
          if(users[i] === ',') {
            user = elements
            html += `<p><i class="fas fa-user"></i> ${user}</p>`;
            elements = ''
          } else {
            elements += users[i]
          } 
        }
        user = elements
        html += `<p><i class="fas fa-user"></i> ${user}</p>`;
        elements = ''
        $users.html(html);
      }

    function checkMessage(data) {
      if(isJson(data)) {
        getMessages(data)
      } else {
        if (data.substring(0,3) === 'nu/') {
          if(data.substring(3) !== 'Taken') {
            $('#nickWrap').hide()
            $('#contentWrap').show()
            getUsernames(data.substring(3))
          } else {
            $nickError.html(`
            <div class="alert alert-danger">
              That username already exist
            </div>
            `)   
          }
        } else {
          $chat.append(`${data}`)
        }
      }
      $messageForm.val('')
    }

    function getMessages (data) {
      const messages = JSON.parse(data)
      for (let i = 0; i < messages.length; i++) {
        $chat.append(`<p><b>${messages[i].nick}</b>: ${messages[i].msg}</p>`);
      }
    }

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
      }

    $messageForm.submit(e => {
      e.preventDefault()
      if (!ws) {
           $chat.append(`<p><b>No WebSocket connection :(</b><p/>`);
      }
      ws.send('msg' + $messageBox.val())
      $messageBox.val('')
    })
  
    $nickForm.submit(e => {
      e.preventDefault()
      console.log('Sending...')
      if (!ws) {
        $nickError.html(`
        <div class="alert alert-danger">
            No WebSocket connection :(
        </div>
        `) 
      return ;
     }
      ws.send(`nu/${$nickname.val()}`)
    })

    init()
  })()
