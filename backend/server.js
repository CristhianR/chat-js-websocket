/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

const express = require('express')
const http = require('http')
const WebSocket = require('ws')
const mongoose = require('mongoose')

const SERVER_PORT = 6969
const DB_PORT = 27017
const DB_SERVER = '127.0.0.1'
const DB_NAME = 'chats'

const server = http.createServer(express)
const wss = new WebSocket.Server({ server })

// Establishing database connection
try {
  mongoose.connect(`mongodb://${DB_SERVER}:${DB_PORT}/${DB_NAME}`,
    { useNewUrlParser: true })
} catch (error) { // On error, close the server
  console.error(error.message)
  process.exit(1)
}

require('./sockets')(wss)

server.listen(SERVER_PORT, function () {
  console.log(`Server is listening on port ${SERVER_PORT}`)
})

module.exports = server
