/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

const WebSocket = require('ws')
const Chat = require('./models/chat')

module.exports = function sockets (wss) {
  const nicknames = []
  const HEADER_START_INDEX_CMD = 0
  const HEADER_END_INDEX_CMD = 3
  const CMD_1 = 'nu/'
  const CMD_2 = 'msg'
  const NICK_TAKEN = 'nu/Taken'

  wss.on('connection', async function connection (socket) {
    console.log('New connection')
    socket.on('message', async function incoming (data) {
      const res = check(data)
      console.log(res)
      if (res.cmd === CMD_2) {
        if (socket.readyState === WebSocket.OPEN) {
          const chat = new Chat({
            msg: res.data,
            nick: socket.nickname
          })
          await chat.save()
        }
        wss.clients.forEach(function each (client) {
          client.send(`<p><b>${socket.nickname}</b>: ${res.data}</p>`)
        })
      } else {
        if (res.data != null) {
          socket.send(socket.nickname)
          wss.clients.forEach(async function each (client) {
            if (client === socket && client.readyState === WebSocket.OPEN) {
              const messages = await Chat.find({})
              client.send(JSON.stringify(messages))
            }
            client.send(res.cmd + nicknames.toString())
          })
        } else {
          socket.send(NICK_TAKEN)
        }
      }
    })

    function check (data) {
      console.log(data)
      if (data.substring(HEADER_START_INDEX_CMD,
        HEADER_END_INDEX_CMD) === CMD_1) {
        if (nicknames.indexOf(data.substring(3)) !== -1) {
          return { cmd: CMD_1, data: null }
        } else {
          socket.nickname = data.substring(3)
          nicknames.push(socket.nickname)
          return { cmd: CMD_1, data: socket.nickname }
        }
      } else {
        if (data.substring(HEADER_START_INDEX_CMD, HEADER_END_INDEX_CMD) ===
         'msg') {
          return { cmd: CMD_2, data: data.substring(3) }
        } else {
          return { cmd: '', data: '' }
        }
      }
    }
    socket.on('close', function disconnect () {
      if (!socket.nickname) {
        return
      }
      nicknames.splice(nicknames.indexOf(socket.nickname), 1)
      wss.clients.forEach(function each (client) {
        if (client.readyState === WebSocket.OPEN) {
          client.send('nu/' + nicknames.toString())
        }
      })
    })
  })
}
