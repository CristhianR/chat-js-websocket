/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

const chai = require('chai')
const expect = chai.expect

const WebSocket = require('ws')

require('../server')

/**
 * Testing login a user into the chat
 */

describe(' Testing commands [nu/, msg] ', () => {
  let ws
  const newUser = 'nu/TEST'
  const message = 'msgTesting message'
  before(() => {
    ws = new WebSocket('ws://localhost:6969')
    ws.onopen = () => {
      console.log('Connection opened! ' + ws.readyState)
      ws.send(newUser)
    }
  })
  it('it should return all the users online + command', async () => {
    ws.onmessage = function (data) {
      if (data.data === newUser.substring(3)) {
        console.log('USER: ' + data.data)
        expect(data.data).to.be.a('string')
        expect(data.data).to.equal(newUser.substring(3))
        ws.send(message)
      }
    }
  })
  it('it should return the message to all the users', async () => {
    ws.onmessage = function (data) {
      console.log('DATA: ' + data.data)
      expect(data.data).to.be.a('string')
    }
  })
})

describe(' msgMessage command ', () => {
  let ws
  const response = '<p><b>TEST</b>: Testing message</p>'
  before(() => {
    ws = new WebSocket('ws://localhost:6969')
    ws.onopen = () => {
      console.log('Connection opened! ' + ws.readyState)
    }
  })
  it('it should return the message to all the users', async () => {
    ws.onmessage = function (data) {
      expect(data.data).to.be.a('string')
      expect(data.data).to.equal(response)
    }
  })
})
